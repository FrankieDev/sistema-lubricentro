        <nav class="side-navbar">
          <!-- Sidebar Header-->
          <div class="sidebar-header d-flex align-items-center">
            <div class="avatar"><img src="img/avatar-1.jpg" alt="..." class="img-fluid rounded-circle"></div>
            <div class="title">
              <h1 class="h4">Luis Castillo</h1>
              <p>Administrador</p>
            </div>
          </div>
          <!-- Sidebar Navidation Menus--><span class="heading">Menú Principal</span>
          <ul class="list-unstyled">
            <li class="<?=$nombre_pagina == "Dashboard" ? "active" : "" ?>"><a href="dashboard.php"> <i class="icon-screen"></i>Dashboard </a></li>
            <li class="<?=$nombre_pagina == "Ordenes" ? "active" : "" ?>"><a href="ordenes.php"> <i class="icon-padnote"></i>Orden de compra </a></li>
            <li class="<?=$nombre_pagina == "Ventas" ? "active" : "" ?>"><a href="ventas.php"> <i class="icon-bill"></i>Ventas </a></li>
            <li class="<?=$nombre_pagina == "Compras" ? "active" : "" ?>"><a href="compras.php"> <i class="icon-bill"></i>Compras </a></li>
            <li class="<?=$nombre_pagina == "Empleados" ? "active" : "" ?>"><a href="empleados.php"> <i class="icon-user"></i>Empleados</a></li>
            <li class="<?=$nombre_pagina == "Productos" ? "active" : "" ?>"><a href="productos.php"> <i class="icon-grid"></i>Productos</a></li>

            <li class="<?=$nombre_pagina == "Usuarios" ? "active" : "" ?>"><a href="usuarios.php"> <i class="icon-user"></i>Usuarios </a></li>
            <li class="<?=$nombre_pagina == "Reportes" ? "active" : "" ?>"><a href="reportes.php"> <i class="fa fa-bar-chart"></i>Reportes </a></li>
            
            <!--
            <li><a href="forms.html"> <i class="icon-padnote"></i>Forms </a></li>
              <li><a href="#exampledropdownDropdown" aria-expanded="false" data-toggle="collapse"> <i class="icon-interface-windows"></i>Example dropdown </a>
              <ul id="exampledropdownDropdown" class="collapse list-unstyled ">
                <li><a href="#">Page</a></li>
                <li><a href="#">Page</a></li>
                <li><a href="#">Page</a></li>
              </ul>
            </li> -->
          </ul>
        </nav>