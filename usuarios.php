<?php 
$nombre_pagina = "Usuarios";
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= $nombre_pagina ?> - Lubricentro</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">

    <?php require_once("header.php") ?>
  
    </head>
  <body>
    <div class="page">

      <!-- Main Navbar-->
      <?php require_once("navbar.php") ?>

      <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
      <?php require_once("sidebar.php") ?>
        
        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom"><?= $nombre_pagina ?></h2>
            </div>
          </header>
          <!-- Breadcrumb-->
          <div class="breadcrumb-holder container-fluid">
            <ul class="breadcrumb">
              <li class="breadcrumb-item"><a href="dashboard.php">Home</a></li>
              <li class="breadcrumb-item active"><?= $nombre_pagina ?></li>
            </ul>
          </div>
          <section class="tables">   
            <div class="container-fluid">
              <div class="row">
                
                <div class="col-lg-12">

                <div class="card">
                    
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Filtros de búsqueda</h3>
                    </div>
                    <div class="card-body">
                      <form class="form-inline">
                      <div class="form-group">
                          <label for="inlineFormInput" class="sr-only">Rut</label>
                          <input id="inlineFormInput" type="text" placeholder="Rut" class="mr-3 form-control">
                        </div>
                        <div class="form-group">
                          <label for="inlineFormInput" class="sr-only">Nombre</label>
                          <input id="inlineFormInput" type="text" placeholder="Nombre" class="mr-3 form-control">
                        </div>
                        <div class="form-group">
                          <label for="inlineFormInputGroup" class="sr-only">Apellido</label>
                          <input id="inlineFormInputGroup" type="text" placeholder="Apellido" class="mr-3 form-control">
                        </div>
                        <div class="form-group select">
                            <select data-style="btn-primary" class="mr-3 form-control " tabindex="-98">
                              <option>Estado</option>  
                              <option>Activo</option>
                              <option>Inactivo</option>
                            </select>
                        </div>

                        <div class="form-group">
                          <button type="submit" class="btn btn-primary">Buscar</button>
                        </div>
                      </form>
                    </div>
                  </div>

                  <div class="card">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                        <div aria-labelledby="closeCard2" class="dropdown-menu dropdown-menu-right has-shadow">
                            <a href="#" class="dropdown-item add" data-toggle="modal" data-target="#modalIngresoMensaje" > <i class="fa fa-plus"></i>Nuevo Usuario</a>
                            <a href="#" class="dropdown-item add" data-toggle="modal" data-target="#modalIngresoMensaje" > <i class="fa fa-plus"></i>Exportar Registros</a>
                        </div>
                      </div>
                    </div>

                    
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4"><?= $nombre_pagina ?></h3>
                    </div>
                    <div class="card-body">
                      <div class="table-responsive">  
                        <table id="tbl-usuarios" class="table table-striped">
                          <thead>
                            <tr>
                                <th>Estado</th>
                                <th>Rut</th>
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>Usuario</th>
                                <th>Fecha Creación</th>
                                <th>Acciones</th>
                            </tr>
                          </thead>
                          
                        </table>
                        
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <!-- Page Footer-->
          <?php require_once("footer.php") ?>

        </div>
      </div>
    </div>
    
    <div id="modalIngresoMensaje" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" aria-hidden="true" class="modal fade text-left">
            <div role="document" class="modal-dialog">
              
              <form id="formMensaje" method="post">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 id="exampleModalLabel" class="modal-title">Nuevo Usuario</h4>
                  <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                  <p>Venta de productos por mesón.</p>
                  
                    <div class="form-group">
                      <label>Titulo</label>
                      <input type="text" id="txtTitulo" name="txtTitulo" placeholder="" class="form-control">
                    </div>
                    <div class="form-group">       
                      <label>Subtitulo</label>
                      <input type="text" id="txtSubTitulo" name="txtSubTitulo" placeholder="" class="form-control">
                    </div>
                    <div class="form-group">       
                      <label>URL</label>
                      <input type="text" id="txtURL" name="txtURL" placeholder="" class="form-control">
                    </div>
                    <div class="form-group">       
                      <label>Mensaje</label>
                      <textarea name="txtMensaje" id="txtMensaje" cols="30" rows="3" placeholder="Ingresa tu mensaje" class="form-control"></textarea>
                    </div>
                  
                </div>
                <div class="modal-footer">
                  <button type="button" data-dismiss="modal" class="btn btn-secondary">Cancelar</button>
                  <button type="submit" class="btn btn-primary">Enviar</button>
                </div>
              </div>
              </form>
            </div>
          </div>
    
    <?php require_once("js.php") ?>    
    <script>
      $(document).ready( function () {
        $("#tbl-usuarios").DataTable({
          "ajax": {
            "url": "admin-ajax.php",
            "type": "post",
            "data": {
              acc: 1
            }
          },
          "language": {
            "url": "/lubricentro/vendor/i18n/Spanish.lang"
          },
          //"serverSide": true,
          //"data": [{"id":"1","rut":"21.946.234-4","nombre":"Francisco","apellido":"Aquino","usuario":"faquino","activo":"1","fecha_creacion":"2018-06-03 00:00:00"}],
          "columns": [
              //{ "data": "activo"},
              { "data": "activo", 
                render: function (data, type, row) {
                  return data == 1 ? "Activo" : "Inactivo";
                }
              },
              { "data": "rut"  },
              { "data": "nombre" },
              { "data": "apellido" },
              { "data": "usuario" },
              { "data": "fecha_creacion" },
              {
                "mData": "accion",
                "mRender": function (data, type, row) {
                    return "<a href='javascript:void(0)' onclick='editarUsuario(" + row.id + ")' class='btn btn-primary btn-sm'><i class='fa fa-edit'></i></a> "
                          +"<a href='javascript:void(0)' onclick='desactivarUsuario(" + row.id + ")' class='btn btn-danger btn-sm'><i class='fa fa-remove'></i></a> "
                          +"<a href='javascript:void(0)' onclick='verDetalle(" + row.id + ")' class='btn btn-info btn-sm'><i class='fa fa-eye'></i></a>";
                }
              }
              /* {
                "class":          "details-control",
                "orderable":      false,
                "data":           null,
                "defaultContent": ""
              }, */
          ]
        });

        // Array to track the ids of the details displayed rows
        var detailRows = [];
        
        $('#tbl-usuarios tbody').on( 'click', 'tr td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = dt.row( tr );
            var idx = $.inArray( tr.attr('id'), detailRows );

            if ( row.child.isShown() ) {
                tr.removeClass( 'details' );
                row.child.hide();

                // Remove from the 'open' array
                detailRows.splice( idx, 1 );
            }
            else {
                tr.addClass( 'details' );
                row.child( format( row.data() ) ).show();

                // Add to the 'open' array
                if ( idx === -1 ) {
                    detailRows.push( tr.attr('id') );
                }
            }
        } )


      })
      
      
    
    </script>
  </body>
</html>