<?php 
$nombre_pagina = "Ordenes";

?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= $nombre_pagina ?> - Lubricentro</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">

    <?php require_once("header.php") ?>
  
    </head>
  <body>
    <div class="page">

      <!-- Main Navbar-->
      <?php require_once("navbar.php") ?>

      <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
      <?php require_once("sidebar.php") ?>
        
        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom"><?= $nombre_pagina ?></h2>
            </div>
          </header>
          <!-- Breadcrumb-->
          <div class="breadcrumb-holder container-fluid">
            <ul class="breadcrumb">
              <li class="breadcrumb-item"><a href="dashboard.php">Home</a></li>
              <li class="breadcrumb-item active"><?= $nombre_pagina ?></li>
            </ul>
          </div>
          <section class="tables">   
            <div class="container-fluid">
              <div class="row">
                
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                        <div aria-labelledby="closeCard2" class="dropdown-menu dropdown-menu-right has-shadow">
                            <a href="#" class="dropdown-item add" data-toggle="modal" data-target="#modalIngresoMensaje" > <i class="fa fa-plus"></i>Agregar</a>
                            
                        </div>
                      </div>
                    </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4"><?= $nombre_pagina ?></h3>
                    </div>
                    <div class="card-body">
                      <div class="table-responsive">  
                        <table class="table table-striped">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th>Titulo</th>
                              <th>Subtitulo</th>
                              <th>Mensaje</th>
                              <th>URL</th>
                              <th>Fecha de envío</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <th scope="row">1</th>
                              <td>Mark</td>
                              <td>Otto</td>
                              <td>@mdo</td>
                              <td><a href="#">http://www.cardiopilates.cl/</a></td>
                              <td>01-06-2018</td>
                            </tr>
                            <tr>
                              <th scope="row">2</th>
                              <td>Jacob</td>
                              <td>Thornton</td>
                              <td>@fat</td>
                              <td><a href="#">http://www.cardiopilates.cl/</a></td>
                              <td>01-06-2018</td>
                            </tr>
                            <tr>
                              <th scope="row">3</th>
                              <td>Larry</td>
                              <td>the Bird</td>
                              <td>@twitter</td>
                              <td><a href="#">http://www.cardiopilates.cl/</a></td>
                              <td>01-06-2018</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <!-- Page Footer-->
          <?php require_once("footer.php") ?>

        </div>
      </div>
    </div>
    
    <div id="modalIngresoMensaje" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" aria-hidden="true" class="modal fade text-left">
            <div role="document" class="modal-dialog">
              
              
              <form id="formMensaje" method="post">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 id="exampleModalLabel" class="modal-title">Agregar Mensaje</h4>
                  <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                  <p>El mensaje serán enviado a todos los dispositivos.</p>
                  
                    <div class="form-group">
                      <label>Titulo</label>
                      <input type="text" id="txtTitulo" name="txtTitulo" placeholder="" class="form-control">
                    </div>
                    <div class="form-group">       
                      <label>Subtitulo</label>
                      <input type="text" id="txtSubTitulo" name="txtSubTitulo" placeholder="" class="form-control">
                    </div>
                    <div class="form-group">       
                      <label>URL</label>
                      <input type="text" id="txtURL" name="txtURL" placeholder="" class="form-control">
                    </div>
                    <div class="form-group">       
                      <label>Mensaje</label>
                      <textarea name="txtMensaje" id="txtMensaje" cols="30" rows="3" placeholder="Ingresa tu mensaje" class="form-control"></textarea>
                    </div>
                  
                </div>
                <div class="modal-footer">
                  <button type="button" data-dismiss="modal" class="btn btn-secondary">Cancelar</button>
                  <button type="submit" class="btn btn-primary">Enviar</button>
                </div>
              </div>
              </form>
            </div>
          </div>
    
    <?php require_once("js.php") ?>    
 
  </body>
</html>