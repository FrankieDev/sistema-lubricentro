<?php
require_once "class.conex.php";

class autentificar{
    private $db;
	private $table;
	private $permisos;
	private $cambioClave;
    var $salt; 

    function autentificar(){
        $this->db = new conexion(); 
		$this->table = "usuario";		
        $this->salt = '7p69tiDcjRKhYJlN1Wf48';         
    }

	//---------------------------------------------------------------------------------------------------------------------

    function login($uname, $pword){
		$this->destruyeSecciones();
		$pass = $this->encriptPass($pword);
		$query = "SELECT * FROM `".$this->table."` WHERE `usu_usuario` = '" .$this->db->db_real_escape_string($uname). "' AND `usu_password` = '". $pass ."' AND usu_activo = 1 LIMIT 1";	
		$result = $this->db->db_query($query);

        if($this->db->db_numrows($result) > 0) { 
			session_name('sessionpush2018');
			if(@session_start() == false){session_destroy();session_start();}
			
			if (!isset($_SESSION['USUARIO_ID'])){
				$datos = $this->db->db_fetch_array($result);
				$_SESSION['USUARIO_NOMBRE'] = $this->db->convertText($datos['usu_nombre']);
				$_SESSION["ULTIMO_ACCESO"] = $datos["usu_ultimo_acceso"];
				$_SESSION['USUARIO_CAMBIO_CLAVE'] = $datos["usu_cambio_clave"];
				$_SESSION['USUARIO_ID'] = $datos['usu_id'];
				$_SESSION['USUARIO_PERFIL'] = $datos['usu_perfil'];

			}			

            return 1; 
        } else {
			$this->destruyeSecciones();
            return 0;
        }       

    } //end login()

	//---------------------------------------------------------------------------------------------------------------------

	private function destruyeSecciones(){
		session_name('sessionpush2018');
		if(@session_start() == false){session_destroy();session_start();}
		
		unset($_SESSION['USUARIO_NOMBRE']);
		unset($_SESSION['ULTIMO_ACCESO']);
		unset($_SESSION['USUARIO_CAMBIO_CLAVE']);
		unset($_SESSION['USUARIO_ID']);
		unset($_SESSION['USUARIO_PERFIL']);
		session_destroy();
		return;	

	}//end destruyeSecciones

	//---------------------------------------------------------------------------------------------------------------------

	function cambioPassword($id, $pwActual, $pwNuevo, $pwNuevoConf){

		if($this->compruebaPassword($id, $pwActual)){
			if(!strcmp($pwNuevo, $pwNuevoConf)){
				$pass = $this->encriptPass($pwNuevo);
				$query = "UPDATE `".$this->table."` SET `usu_password` = '".$pass."', `usu_cambio_clave` = 0 WHERE `usu_id` ='".intval($id)."' AND usu_activo = 1;";
				$result = $this->db->db_query($query);
				$_SESSION['USUARIO_CAMBIO_CLAVE'] = 0;
			echo "var resp = 'OK'";
			}else{
			echo "var resp = 'FAIL'";
			}

		}else{
			echo "var resp = 'NOIGUAL'";
		}

	}

	//---------------------------------------------------------------------------------------------------------------------

	private function compruebaPassword($id, $pword){	

		$pass = $this->encriptPass($pword);
		$query = "SELECT usu_id FROM `".$this->table."` WHERE `usu_id` ='".intval($id)."' AND `usu_password` = '".$pass."' AND usu_activo = 1 LIMIT 1";		
		$result = $this->db->db_query($query);
        if($this->db->db_numrows($result) > 0){
			return true;
		}else{
			return false;
		}

	}

	//---------------------------------------------------------------------------------------------------------------------

	function olvidoContrasenna($email){

		$sql = "SELECT * FROM `".$this->table."` WHERE `usu_email` = '".$email."' AND usu_activo = 1  LIMIT 1";		
		$resp = $this->db->db_query($sql);
		$numRow = $this->db->db_numrows($resp);

		if($numRow == 0){
			return 0;
		}else{
			$row = $this->db->db_fetch_array($resp);	
			$str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
			$cad = "";

			for($i=0;$i<6;$i++) {
				$cad .= substr($str,rand(0,62),1);
			}

			$newPasswd = $cad;
			$pass = $this->encriptPass($newPasswd);

			$change = "UPDATE `".$this->table."` SET `usu_password` = '".$pass."', `usu_cambio_clave` = 1 WHERE `usu_id` ='".$row['usu_id']."' LIMIT 1;";

			if($this->db->db_query($change)){				
				return $newPasswd;
			}else{
				return  false;
			}
			
		}		
	}
	
	//---------------------------------------------------------------------------------------------------------------------
	
	function encriptPass($pass){		
		return  base64_encode(hash_hmac('md5', $pass, $this->salt));
	}

	//---------------------------------------------------------------------------------------------------------------------

	private function Close(){
		@mysql_free_result($this->result);
		@mysql_close($this->conex);
	}//end close

}

?>